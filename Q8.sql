﻿select
i.item_id,
i.item_name,
i.item_price,
ic.category_name
from
item i
inner join
item_category ic
on
i.category_id = ic.category_id
